                      "foldershare_rest" module for Durpal 8

                      by the San Diego Supercomputer Center
                   at the University of California at San Diego


CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------
The FolderShare REST module for Drupal 8 provides REST web services for the
separate FolderShare module. This enables users to use non-browser client
applications and scripts to access files and folders managed by FolderShare.


REQUIREMENTS
------------
The FolderShare REST module requires Drupal 8.7 or greater. It requires the
FolderShare module, which manages files and folders, and the core
RESTful Web Services module to provide the REST framework. The latter module
requires core's Serialization module.

The module does not require any third-party contributed modules or libraries.
The third-party REST UI module is recommended to enable a UI for configuring
this module's REST plugin.

See foldershare.info.yml for a list of required modules.


INSTALLATION
------------
Install the module as you would normally install a contributed Drupal module.
Visit:
  https://www.drupal.org/docs/user_guide/en/config-install.html


CONFIGURATION
-------------
There is very little to configure. If you have the REST UI module installed,
you can view this module's REST plugin configuration. That configuration
defaults to:
	- All HTTP methods enabled.
	- JSON serialization.
	- Cookie authentication.

This module supports Drupal core's default Cookie Authentication as the
preferred mechanism for authenticating users. Drupal core's HTTP Basic
Authentication is supported, but not recommended, due to bugs in that module.

This module requires JSON serialization. JSON+HAL serialization is also
supported, but the extra HAL features are unused. XML serialization is not
supported.


ADDITIONAL HELP
---------------
If the "Help" module is installed, FolderShare REST's help page provides
additional information.


MAINTAINERS
-----------
Current maintainers:
 * Amit Chourasia
   San Diego Supercomputer Center
   University of California at San Diego

This project has been sponsored by:
 * The National Science Foundation. See NSF.txt
