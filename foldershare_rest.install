<?php

/**
 * @file
 * Handles module installation, uninstallation, and version updates.
 */

/**
 * @defgroup foldershare_rest FolderShare REST
 *
 * @{
 * This module provides REST web services for the FolderShare module.
 * This enables users to use non-browser client applications and scripts
 * to access the site and upload, download, create, delete, and modify shared
 * files and folders managed by FolderShare.
 * @}
 */

/**
 * Checks installation requirements for the indicated phase.
 *
 * @param string $phase
 *   The phase of installation being checked. Known values are "install",
 *   "update", and "runtime".
 */
function foldershare_rest_requirements($phase) {
  // Insure we have a reasonable phase.
  switch ($phase) {
    case 'install':
    case 'update':
    case 'runtime':
      break;

    default:
      $phase = 'runtime';
      break;
  }

  $requirements = [];

  // Confirm the PHP ZIP extension is available.
  if (extension_loaded('zip') === FALSE) {
    $title = t('FolderShare REST PHP extensions');
    $value = t('Disabled');

    $description = t(
      'The FolderShare REST module requires the PHP "zip" extension in order to support downloads of .zip files. Attempts to use this functionality will cause errors. Installing this PHP extension is strongly recommended.');

    $requirements['foldershare_rest_php_extensions'] = [
      'title'       => $title,
      'value'       => $value,
      'description' => $description,
      'severity'    => REQUIREMENT_WARNING,
    ];
  }

  return $requirements;
}

require 'foldershare_rest.update.inc';
