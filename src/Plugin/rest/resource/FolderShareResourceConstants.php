<?php

namespace Drupal\foldershare_rest\Plugin\rest\resource;

/**
 * Defines constants for the FolderShareResource class.
 */
final class FolderShareResourceConstants {
  /*--------------------------------------------------------------------
   *
   * Constants - Protocol version.
   *
   *--------------------------------------------------------------------*/
  /**
   * Indicates the client-server protocol version number.
   *
   * The protocol version number is sent from the server to the client
   * during initial contact handshaking. If the client cannot support the
   * current version, it reports an error to the user.
   *
   * @var float
   */
  const PROTOCOL_VERSION = 1.2;

  /*--------------------------------------------------------------------
   *
   * Constants - Special entity IDs.
   *
   *--------------------------------------------------------------------*/
  /**
   * Indicates that no FolderShare item ID was provided.
   *
   * @var int
   */
  const EMPTY_ITEM_ID = (-1);

  /**
   * Indicates that no user ID was provided.
   *
   * @var int
   */
  const EMPTY_USER_ID = (-1);

  /*--------------------------------------------------------------------
   *
   * Constants - custom header fields.
   *
   * These header fields are recognized by this resource to selection
   * among different operations available or provide guidance on how
   * to perform an operation or return results.
   *
   *--------------------------------------------------------------------*/
  /**
   * A custom request header specifying a GET operation.
   *
   * Header values are any of those listed in GET_OPERATIONS.
   *
   * The default operation value is "entity".
   *
   * @see self::GET_OPERATIONS
   */
  const HEADER_GET_OPERATION = "X-FolderShare-Get-Operation";

  /**
   * A custom request header specifying a DELETE operation.
   *
   * Header values are any of those listed in DELETE_OPERATIONS.
   *
   * The default operation value is "entity".
   *
   * @see self::DELETE_OPERATIONS
   */
  const HEADER_DELETE_OPERATION = "X-FolderShare-Delete-Operation";

  /**
   * A custom request header specifying a POST operation.
   *
   * Header values are any of those listed in POST_OPERATIONS.
   *
   * The default operation value is "entity".
   *
   * @see self::POST_OPERATIONS
   */
  const HEADER_POST_OPERATION = "X-FolderShare-Post-Operation";

  /**
   * A custom request header specifying a PATCH operation.
   *
   * Header values are any of those listed in PATCH_OPERATIONS.
   *
   * The default operation value is "entity".
   *
   * @see self::PATCH_OPERATIONS
   */
  const HEADER_PATCH_OPERATION = "X-FolderShare-Patch-Operation";

  /**
   * A custom request header specifying a GET's search scope.
   *
   * Header values are one or more of those listed in SEARCH_SCOPES.
   * When multiple values are used, they should be listed and separated
   * by commas.
   *
   * The default search scope is "name, body".
   *
   * @see self::SEARCH_SCOPES
   */
  const HEADER_SEARCH_SCOPE = "X-FolderShare-Search-Scope";

  /**
   * A custom request header specifying a return type.
   *
   * Header values are any of those listed in RETURN_FORMATS.
   *
   * The default return type is "entity".
   *
   * @see self::RETURN_FORMATS
   */
  const HEADER_RETURN_FORMAT = "X-FolderShare-Return-Format";

  /**
   * A custom request header to specify a source entity path.
   *
   * Header values are strings for a root-to-leaf path to a file or
   * folder. Strings must be URL encoded in order for them to include
   * non-ASCII characters in an HTTP header.
   *
   * For operations that operate upon an entity, if the header is present
   * the path is used to find the entity instead of using an entity ID
   * on the route.
   */
  const HEADER_SOURCE_PATH = "X-FolderShare-Source-Path";

  /**
   * A custom request header to specify a destination entity path.
   *
   * Header values are strings for a root-to-leaf path to a file or
   * folder. Strings must be URL encoded in order for them to include
   * non-ASCII characters in an HTTP header.
   *
   * For operations that operate upon an entity, if the header is present
   * the path is used to find the entity instead of using an entity ID
   * on the route.
   */
  const HEADER_DESTINATION_PATH = "X-FolderShare-Destination-Path";

  /**
   * A custom request header to specify a user name or ID.
   *
   * Header values are strings that specify a user name or an integer
   * user ID. Strings must be URL encoded in order for them to include
   * non-ASCII characters in an HTTP header.
   */
  const HEADER_USER = "X-FolderShare-User";

  /*--------------------------------------------------------------------
   *
   * Constants - header values.
   *
   * These are well-known values for the custom header fields above.
   *
   *--------------------------------------------------------------------*/
  /**
   * A list of well-known GET operation names.
   *
   * These names may be used as values for the self::HEADER_GET_OPERATION
   * HTTP header.
   *
   * @see self::HEADER_GET_OPERATION
   * @see self::DEFAULT_GET_OPERATION
   */
  const GET_OPERATIONS = [
    // GET operations that can use an entity ID.
    'get-entity',
    'get-parent',
    'get-root',
    'get-ancestors',
    'get-descendants',
    'get-sharing',
    'search',
    'download',

    // GET operations for configuration settings.
    'get-configuration',
    'get-usage',
    'get-version',
  ];

  /**
   * The default GET operation.
   *
   * @see self::GET_OPERATIONS
   */
  const DEFAULT_GET_OPERATION = 'get-entity';

  /**
   * A list of well-known DELETE operation names.
   *
   * These names may be used as values for the self::HEADER_DELETE_OPERATION
   * HTTP header.
   *
   * @see self::HEADER_DELETE_OPERATION
   * @see self::DEFAULT_DELETE_OPERATION
   */
  const DELETE_OPERATIONS = [
    // DELETE operations never take an entity ID, just a path.
    'delete-file',
    'delete-folder',
    'delete-folder-tree',
    'delete-file-or-folder',
    'delete-file-or-folder-tree',
  ];

  /**
   * The default DELETE operation.
   *
   * @see self::DELETE_OPERATIONS
   */
  const DEFAULT_DELETE_OPERATION = 'delete-file';

  /**
   * A list of well-known POST operation names.
   *
   * These names may be used as values for the self::HEADER_POST_OPERATION
   * HTTP header.
   *
   * @see self::HEADER_POST_OPERATION
   * @see self::DEFAULT_POST_OPERATION
   */
  const POST_OPERATIONS = [
    // POST operations that may take an entity ID.
    'new-rootfolder',
    'new-folder',
    'new-file',
    'new-file-no-overwrite',
    'new-rootfile',
    'new-rootfile-no-overwrite',
    'new-media',
    'new-object',
  ];

  /**
   * The default POST operation.
   *
   * @see self::POST_OPERATIONS
   */
  const DEFAULT_POST_OPERATION = 'new-rootfolder';

  /**
   * A list of well-known PATCH operation names.
   *
   * These names may be used as values for the self::HEADER_PATCH_OPERATION
   * HTTP header.
   *
   * @see self::HEADER_PATCH_OPERATION
   * @see self::DEFAULT_PATCH_OPERATION
   */
  const PATCH_OPERATIONS = [
    // PATCH operations that can use an entity ID.
    'update-entity',
    'update-sharing',

    // PATCH operations that need a destination.
    'copy-overwrite',
    'copy-no-overwrite',
    'move-overwrite',
    'move-no-overwrite',

    // PATCH operations that need a user ID.
    'change-owner',
    'change-owner-recursive',
  ];

  /**
   * The default PATCH operation.
   *
   * @see self::PATCH_OPERATIONS
   */
  const DEFAULT_PATCH_OPERATION = 'update-entity';

  /**
   * A list of well-known return types.
   *
   * These types may be used as values for the self::HEADER_RETURN_FORMAT
   * HTTP header that is supported for most GET, POST, and PATCH operations.
   *
   * @see self::HEADER_RETURN_FORMAT
   * @see self::DEFAULT_RETURN_FORMAT
   */
  const RETURN_FORMATS = [
    'full',
    'keyvalue',
  ];

  /**
   * The default return type.
   *
   * @see self::RETURN_FORMATS
   */
  const DEFAULT_RETURN_FORMAT = 'full';

  /**
   * A list of well-known search scopes.
   *
   * These values may be used for the self::HEADER_SEARCH_SCOPE HTTP header
   * that is supported by the GET "search" operation.
   *
   * @see self::HEADER_GET_OPERATION
   * @see self::GET_OPERATIONS
   * @see self::DEFAULT_SEARCH_SCOPE
   */
  const SEARCH_SCOPES = [
    'name',
    'body',
    'file-content',
  ];

  /**
   * The default search scope.
   *
   * @see self::SEARCH_SCOPES
   */
  const DEFAULT_SEARCH_SCOPE = 'name,body';

}
