<?php

namespace Drupal\foldershare_rest\Plugin\rest\resource;

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginManagerInterface;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\File\FileSystem;
use Drupal\Core\ProxyClass\File\MimeType\MimeTypeGuesser;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\Plugin\rest\resource\EntityResourceValidationTrait;

use Psr\Log\LoggerInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

use Drupal\foldershare\Entity\FolderShare;

use Drupal\foldershare_rest\Plugin\rest\resource\FolderShareResourceTraits\ConfigureTrait;
use Drupal\foldershare_rest\Plugin\rest\resource\FolderShareResourceTraits\ManageRequestTrait;
use Drupal\foldershare_rest\Plugin\rest\resource\FolderShareResourceTraits\ManageResponseTrait;
use Drupal\foldershare_rest\Plugin\rest\resource\FolderShareResourceTraits\GetResponseTrait;
use Drupal\foldershare_rest\Plugin\rest\resource\FolderShareResourceTraits\DeleteResponseTrait;
use Drupal\foldershare_rest\Plugin\rest\resource\FolderShareResourceTraits\PostResponseTrait;
use Drupal\foldershare_rest\Plugin\rest\resource\FolderShareResourceTraits\PatchResponseTrait;

/**
 * Responds to FolderShare entity REST GET, POST, PATCH, and DELETE requests.
 *
 * This REST resource provides a web services response for operations
 * related to FolderShare entities. This includes responses to the following
 * standard HTTP requests:
 * - GET: Get a FolderShare entity, list of entities, state, or module settings.
 * - POST: Create a FolderShare entity.
 * - PATCH: Modify a FolderShare entity's fields or sharing settings.
 * - DELETE: Delete a FolderShare entity.
 *
 * FolderShare entities are hierarchical and create a tree of files and
 * folders nested within other folders to arbitrary depth (see the
 * FolderShare entity). Working with this hierarchical necessarily requires
 * that GET operations go beyond requesting a single entity's values based
 * upon that single entity's ID. Instead, this REST resource supports
 * multiple types of GET operations that can request lists of entities,
 * the descendants or ancestors of an entity, the path to an entity, and
 * the sharing settings for a folder tree. Additional GET operations can
 * trigger a search of a folder tree and get non-entity values, such as
 * module settings and usage. Finally, GET operations can download an
 * entity to the local host.
 *
 * POST operations can create a new folder as a top-level folder, or as a
 * child of a specified folder. POST operations can also upload a file or
 * folder, copy a folder tree, or create an archive from a file or folder tree.
 *
 * PATCH operations can modify the values of an entity, such as its name
 * or description, and also move the entity to a different part of the
 * folder tree.
 *
 * DELETE operations can delete a file or folder, including any child files
 * and folders.
 *
 * <B>Routes and URLs</B><BR>
 * GET, PATCH, and DELETE operations all use a canonical URL that must
 * include an entity ID, even for operations that do not require an entity ID.
 * This is an artifact of the REST module's requirement that they all share
 * the same route, and therefore have the same URL structure.
 *
 * POST operations all use a special creation URL that cannot include an
 * entity ID, even for operations that need one, such as creating a subfolder
 * within a selected parent folder. The lack of an entity ID in the URL is
 * an artifact of the REST module's assumption that entities are not related
 * to each other.
 *
 * Since most of FolderShare's GET, POST, and PATCH operations require
 * additional information beyond what is included (or includable) on the URL,
 * this response needs another mechanism for providing that additional
 * information. While POST can receive posted data, GET and PATCH cannot.
 * This leaves us with two possible design solutions:
 * - Add URL query strings.
 * - Add HTTP headers.
 *
 * Both of these designs are possible, but the use of URL query strings is
 * cumbersome for the range and complexity of the information needed. This
 * design therefore uses several custom HTTP headers to provide
 * this information. Most headers are optional and defaults are used if
 * the header is not available.
 *
 * When paths are used, a path has one of these forms:
 * - "DOMAIN:/PATH" = a path relative to a specific content domain.
 * - "/PATH" = a path relative to the "pesonal" content domain.
 *
 * The following domains are recognized:
 * - "pesonal" = (default) the user's own files and folders and those
 *   shared with them.
 * - "public" = the site's public files and folders.
 *
 * The following minimal paths have special meanings:
 * - "pesonal:/" = the user's own root list and those shared with them.
 * - "public:/" = the site's public root list.
 *
 * Some operations require a path to a specific file or folder. In these
 * cases, the operation will return an error with minimal paths that
 * refer to domains of content, rather than a specific entity.
 *
 * <B>Headers</B><BR>
 * Primary headers specify an operation to perform:
 *
 * - X-FolderShare-Get-Operation selects the GET variant, such as whether
 *   to get a single entity, a list of entities, or module settings.
 *
 * - X-FolderShare-Post-Operation selects the POST variant, such as whether
 *   to create a new folder, upload a file, copy a folder
 *   tree, or create an archive.
 *
 * - X-FolderShare-Patch-Operation selects the PATCH variant, such as whether
 *   to modify an entity's fields or move the entity.
 *
 * - X-FolderShare-Delete-Operation selects the DELETE variant.
 *
 * Many GET, PATCH, and DELETE operations require a source, or subject entity
 * to operate upon. By default, the entity is indicated by a non-negative
 * integer entity ID included on the URL. Alternately, the entity may be
 * specified via a folder path string included in the header:
 *
 * - X-FolderShare-Source-Path includes the URL encoded entity path.
 *   When present, this path overrides the entity ID in the URL.
 *
 * Some PATCH operations copy or move an entity to a new location. That
 * location is always specified via a folder path string in another header:
 *
 * - X-FolderShare-Destination-Path includes the URL encoded entity
 *   path to the destination.
 *
 * GET, POST, and PATCH can return information to the client. The structure
 * of that information may be controlled using:
 *
 * - X-FolderShare-Return-Format selects whether to return a serialized entity,
 *   a simplified key-value pair array, an entity ID, a path, or a "Linux"
 *   formatted result.
 *
 * @internal
 * <B>Collision with default entity response</B><BR>
 * The Drupal REST module automatically creates a resource for every entity
 * type, including FolderShare. However, its implementation presumes simple
 * entities that have no linkage into a hierarchy and no context-sensitive
 * constraints on names and other values. If the default response were used,
 * a malicious user could overwrite internal entity fields (like parent,
 * root, and file IDs) and corrupt the hierarchy.  This class therefore
 * provides a proper REST response for FolderShare entities and insures that
 * the hierarchy is not corrupted.
 *
 * However, we need to insure that the default resource created by the REST
 * module is disabled and <B>not available</B>. If it were available, a site
 * admin could enable it without understanding it's danger.
 *
 * There is no straight-forward way to block a default REST resource. It is
 * created at Drupal boot and added to the REST plugin manager automatically.
 * It remains in the plugin manager even if we define a custom resource for
 * the same entity type. However, we can <B>block the default resource</B>
 * by giving our resource the same plugin ID. This overwrites the default
 * resource's entry in the plugin manager's hash table, leaving only this
 * resource visible.
 *
 * The plugin ID for this resource is therefore: "entity:foldershare"
 * to intentionally overwrite the default resource. This is mandatory!
 *
 * <B>Link relation types of URI paths</B>
 * The URI path keys are supposed to be link relation types, as defined by
 * IETF RFC5988. They may also use any of the registered extensions (and
 * there are a lot).
 *
 * Unfortunately, this is a misuse of link relation types. The URLs here
 * must be matched with corresponding HTTP verbs supported by Drupal's
 * REST module:
 * - GET to get information.
 * - POST to create new information.
 * - PATCH to update existing information.
 * - DELETE to delete existing information.
 *
 * The REST module recognizes a single "canonical" path which is typically
 * associated with GET. This class uses this path in this way.
 *
 * There is a core "create" link relation type in RFC5988, so it is used
 * for POST. This is also a shown in REST module examples.
 *
 * But there is no "delete" link relation type in RFC5988. The closest matches
 * are "delete-form" and "delete-multiple-form". Neither seems like a good
 * match since this is a REST resource and there is no form. Fortunately,
 * link relation type names are not constrained to RFC5988, so this class
 * uses "delete".
 *
 * There is no "patch", "update", or "change" link relation type, but there
 * are registered extension types for "edit", "edit-form", and "edit-media".
 * This class uses "edit".
 *
 * @see https://tools.ietf.org/html/rfc5988#section-4.2
 * @see https://www.iana.org/assignments/link-relations/link-relations.xhtml
 * @see https://api.drupal.org/api/drupal/core%21core.link_relation_types.yml/8.7.x
 *
 * @ingroup foldershare_rest
 *
 * @RestResource(
 *   id    = "entity:foldershare",
 *   label = @Translation("FolderShare files and folders"),
 *   serialization_class = "Drupal\foldershare\Entity\FolderShare",
 *   uri_paths = {
 *     "canonical" = "/foldershare/{id}",
 *     "delete" = "/entity/foldershare",
 *     "create" = "/entity/foldershare",
 *     "edit" = "/entity/foldershare"
 *   }
 * )
 */
class FolderShareResource extends ResourceBase implements DependentPluginInterface {
  use EntityResourceValidationTrait;
  use ConfigureTrait;
  use ManageRequestTrait;
  use ManageResponseTrait;
  use GetResponseTrait;
  use DeleteResponseTrait;
  use PostResponseTrait;
  use PatchResponseTrait;
  /*--------------------------------------------------------------------
   *
   * Fields.
   *
   *--------------------------------------------------------------------*/
  /**
   * The entity type targeted by this resource.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $definition;

  /*--------------------------------------------------------------------
   *
   * Fields - dependency injection.
   *
   *--------------------------------------------------------------------*/
  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * The link relation type manager used to create HTTP header links.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $linkRelationTypeManager;

  /**
   * The MIME type guesser.
   *
   * @var Drupal\Core\ProxyClass\File\MimeType\MimeTypeGuesser
   */
  protected $mimeTypeGuesser;

  /**
   * The list of installed modules.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleList;

  /**
   * The configuration for this REST resource.
   *
   * @var \Drupal\rest\RestResourceConfigInterface
   */
  protected $resourceConfiguration;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $typeManager;

  /*--------------------------------------------------------------------
   *
   * Construct.
   *
   *--------------------------------------------------------------------*/
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the REST
   *   plugin instance.
   * @param string $pluginId
   *   The ID for the REST plugin instance.
   * @param mixed $pluginDefinition
   *   The REST plugin implementation definition.
   * @param Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $typeManager
   *   The entity type manager.
   * @param array $serializerFormats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Symfony\Component\HttpFoundation\Request $currentRequest
   *   The current HTTP request.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $linkRelationTypeManager
   *   The link relation type manager.
   * @param \Drupal\Core\File\FileSystem $fileSystem
   *   The file system service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleList
   *   The list of installed modules.
   * @param Drupal\Core\File\ProxyClass\MimeType\MimeTypeGuesser $mimeTypeGuesser
   *   The MIME type guesser.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    AccountProxyInterface $currentUser,
    EntityTypeManagerInterface $typeManager,
    array $serializerFormats,
    LoggerInterface $logger,
    Request $currentRequest,
    PluginManagerInterface $linkRelationTypeManager,
    FileSystem $fileSystem,
    ModuleExtensionList $moduleList,
    MimeTypeGuesser $mimeTypeGuesser) {

    parent::__construct(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $serializerFormats,
      $logger);

    //
    // Notes:
    // - $configuration is required as an argument to the parent class,
    //   but it is empty. This is *not* the resource configuration.
    //
    // - $serializerFormats is required as an argument to the parent class,
    //   but it lists *all* formats installed at the site, rather than just
    //   those configured as supported by this plugin.
    //
    // Get the FolderShare entity definition. This is needed later
    // to calculate dependencies. Specifically, this resource is dependent
    // upon the FolderShare entity being installed.
    $this->definition = $typeManager->getDefinition(
      FolderShare::ENTITY_TYPE_ID);

    // Get the resource's configuration. The configuration is named
    // after this resource plugin, replacing ':' with '.'.
    $restResourceStorage = $typeManager->getStorage('rest_resource_config');
    $this->resourceConfiguration = $restResourceStorage->load('entity.foldershare');

    // And save information for later.
    $this->currentUser = $currentUser;
    $this->typeManager = $typeManager;
    $this->currentRequest = $currentRequest;
    $this->linkRelationTypeManager = $linkRelationTypeManager;
    $this->fileSystem = $fileSystem;
    $this->moduleList = $moduleList;
    $this->mimeTypeGuesser = $mimeTypeGuesser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition) {

    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('plugin.manager.link_relation_type'),
      $container->get('file_system'),
      $container->get('extension.list.module'),
      $container->get('file.mime_type.guesser')
    );
  }

}
