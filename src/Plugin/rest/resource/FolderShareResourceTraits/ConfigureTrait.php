<?php

namespace Drupal\foldershare_rest\Plugin\rest\resource\FolderShareResourceTraits;

use Drupal\rest\RequestHandler;

use Symfony\Component\Routing\Route;

use Drupal\foldershare\Entity\FolderShare;

/**
 * Respond to queries about the REST resource configuration.
 *
 * This trait includes methods required by a REST resource implementation
 * to report on dependencies, permissions, and the base route for
 * communications.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare REST module and used to define
 * features of the FolderShareResource entity class. It is a mechanism to group
 * functionality to improve code management.
 *
 * @ingroup foldershare_rest
 */
trait ConfigureTrait {

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    if (isset($this->definition) === TRUE) {
      return ['module' => [$this->definition->getProvider()]];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function permissions() {
    // Older versions of Drupal allowed REST implementations to define
    // additional permissions for REST access. Versions of Drupal >= 8.2
    // drop REST-specific entity permissions and instead use the entity's
    // regular permissions.
    //
    // Since the FolderShare module requires Drupal 8.4 or greater, the
    // older REST permissions model is not needed and we return nothing.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseRoute($canonicalPath, $method) {
    switch ($method) {
      default:
      case 'GET':
        // GET requests use the canonical path, which includes an optional
        // entity ID in the route.
        $route = parent::getBaseRoute($canonicalPath, $method);

        if ($route->getOption('parameters') === FALSE) {
          $parameters = [];
          $parameters[FolderShare::ENTITY_TYPE_ID]['type'] =
            'entity:' . FolderShare::ENTITY_TYPE_ID;
          $route->setOption('parameters', $parameters);
        }
        break;

      case 'PATCH':
        // PATCH uses the "edit" named path. The path does not include
        // an entity ID in the path, though it may be included in a
        // query string "?id=ID".
        //
        // PATCHes are used to modify files and folders, such as to change
        // a description or other field. Web clients normally use user-friendly
        // file paths, not entity IDs (e.g. "/myfolder/myfile.txt") so
        // the client usually does not have an entity ID to pass in a URL.
        // Since the canonical path used by GET requires an entity ID,
        // PATCH must use a different path.
        //
        // PATCH uses HTTP headers that give the paths to items to edit.
        // If the headers are not present, these fall back to an ID in the
        // query string. And if that is not present, an error is returned.
        $route = new Route(
          "/entity/foldershare",
          [
            // The "handle" handler passes the raw request and the
            // unserialized incoming dummy entity to the patch() method.
            '_controller' => RequestHandler::class . '::handle',
          ],
          $this->getBaseRouteRequirements($method),
          [],
          '',
          [],
          // The HTTP method is a requirement for this route. This is what
          // sends incoming requests to the patch() vs. delete() method.
          [$method]
        );
        break;

      case 'DELETE':
        // DELETE uses the "delete" named path. The path does not include
        // an entity ID in the path, though it can be included in a query
        // string "?id=ID".
        //
        // DELETEs are used to delete files and folders. Web clients normally
        // use user-friendly file paths, not entity IDs (e.g.
        // "/myfolder/myfile.txt") so the client usually does not have an
        // entity ID to pass in a URL. Since the canonical path used by GET
        // requires an entity ID, DELETE must use a different path.
        //
        // DELETE uses HTTP headers that give the paths to items to edit.
        // If the headers are not present, these fall back to an ID in the
        // query string. And if that is not present, an error is returned.
        $route = new Route(
          "/entity/foldershare",
          [
            // The "handleRaw" handler passes the raw request only to
            // the delete() method. There is no entity ID or unserialized
            // entity for a DELETE.
            '_controller' => RequestHandler::class . '::handleRaw',
          ],
          $this->getBaseRouteRequirements($method),
          [],
          '',
          [],
          // The HTTP method is a requirement for this route. This is what
          // sends incoming requests to the patch() vs. delete() method.
          [$method]
        );
        break;

      case 'POST':
        // POST uses the "create" path, which does not include an entity ID
        // in the URL.
        //
        // POSTs are used to create new folders and new files. In both cases,
        // the new thing has a name and destination set by HTTP headers.
        // There is no entity ID yet, so there is nothing that could be passed.
        //
        // A POSTed new file also needs file content. To get at that file
        // content as an input stream, we need the REST resource to *not*
        // try to parse it as form data and create a dummy entity, as it
        // does for a PATCH.
        //
        // To keep POSTs from parsing content, we use "handleRaw" in the
        // request handler. This causes our post() method to be called
        // with the raw request, from which we can get the input stream
        // for an uploaded file's bytes.
        $route = new Route(
          $canonicalPath,
          [
            '_controller' => RequestHandler::class . '::handleRaw',
          ],
          $this->getBaseRouteRequirements($method),
          [],
          '',
          [],
          // The HTTP method is a requirement for this route.
          [$method]
        );
        break;
    }

    return $route;
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseRouteRequirements($method) {
    // Get the base requirements, which include invoking the access
    // controller to verify permissions.
    $req = parent::getBaseRouteRequirements($method);

    if ($method === 'POST') {
      // POST requests create new folders or files.
      //
      // For a new folder, there is no content - just header fields that
      // select the destination and name.
      //
      // For a new file, header fields again select the destination and
      // name, and then the file to upload is attached as a byte stream.
      // The content type of that stream is binary (really
      // "application/octet-stream").
      $req['_content_type_format'] = 'bin';
    }

    // The other methods (GET, PATCH, and DELETE) can use serialized
    // input, such as entities or parameters in JSON.
    return $req;
  }

}
