<?php

namespace Drupal\foldershare_rest\Plugin\rest\resource\FolderShareResourceTraits;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldItemInterface;

use Drupal\user\Entity\User;

use Symfony\Component\HttpFoundation\Response;

use Drupal\foldershare\FolderShareInterface;

use Drupal\foldershare_rest\Plugin\rest\resource\UncacheableResponse;

/**
 * Manage HTTP responses.
 *
 * This trait includes methods that help build a response to a request,
 * including responses with embedded entities and lists of entities.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare REST module and used to define
 * features of the FolderShareResource entity class. It is a mechanism to group
 * functionality to improve code management.
 *
 * @ingroup foldershare_rest
 */
trait ManageResponseTrait {

  /*--------------------------------------------------------------------
   *
   * Utilities.
   *
   *--------------------------------------------------------------------*/
  /**
   * Returns a generic access denied message.
   *
   * When performing access control checks, an access denied response
   * may or may not include a response message. If it does, that message
   * is used. But if not, this method fills in a generic message.
   *
   * @param string $operation
   *   The name of the operation that is not allowed (e.g. 'view').
   *
   * @return string
   *   Returns a generic default message for an AccessDeniedHttpException.
   */
  private function getDefaultAccessDeniedMessage($operation) {
    return "You are not authorized to {$operation} this item.";
  }

  /*--------------------------------------------------------------------
   *
   * Response header utilities.
   *
   *--------------------------------------------------------------------*/
  /**
   * Adds link headers to a response.
   *
   * If the entity has any entity reference fields, this method loops
   * through them and adds them to a 'Link' header field.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The FolderShare entity.
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   The response.
   *
   * @see https://tools.ietf.org/html/rfc5988#section-5
   */
  private function addLinkHeaders(
    EntityInterface $entity,
    Response $response) {

    // Loop through all URI relationships (entity references) in the entity.
    foreach ($entity->uriRelationships() as $relationName) {
      // If the relationship does not have a definition, skip it.
      // We don't have enough information to describe this relationship.
      $hasDef = $this->linkRelationTypeManager->hasDefinition($relationName);
      if ($hasDef === FALSE) {
        continue;
      }

      // Get the relationship information.
      $type = $this->linkRelationTypeManager->createInstance($relationName);

      if ($type->isRegistered() === TRUE) {
        $relationship = $type->getRegisteredName();
      }
      else {
        $relationship = $type->getExtensionUri();
      }

      // Generate a URL for the relationship.
      $urlObj = $entity->toUrl($relationName)
        ->setAbsolute(TRUE)
        ->toString(TRUE);

      $url = $urlObj->getGeneratedUrl();

      // Add a link header for this item.
      $response->headers->set(
        'Link',
        '<' . $url . '>; rel="' . $relationship . '"',
        FALSE);
    }
  }

  /*--------------------------------------------------------------------
   *
   * Raw entity pseudo-serialization.
   *
   *--------------------------------------------------------------------*/
  /**
   * Updates a raw entity version of an entity.
   *
   * This method adds virtual fields to the entity that assist a REST
   * client in intepreting and using the entity's values:
   *
   * - ['host'][0]['value'] = the current host name.
   * - ['path'][0]['value'] = the entity's absolute path.
   * - ['sharing-status'][0]['value'] = the entity's sharing status.
   *
   * This method also augments existing fields with alternate information:
   *
   * - ['created'][0]['formatted'] = the ISO 8601 formatted creation date.
   * - ['changed'][0]['formatted'] = the ISO 8601 formatted changed date.
   * - ['uid'][0]['account-name'] = the owner's account name.
   * - ['uid'][0]['display-name'] = the owner's display name.
   *
   * The entity should already have been cleaned to remove empty fields and
   * fields that cannot be viewed by the user.
   *
   * See formatKeyValueEntity() for a discussion of formatted timestamps.
   *
   * @param \Drupal\foldeshare\FolderShareInterface $entity
   *   The raw entity.
   *
   * @return array
   *   Returns the raw entity array with added virtual fields.
   */
  private function formatRawEntity(FolderShareInterface &$entity) {
    $content = $entity->toArray();

    // Add virtual fields:
    //
    // - The current host name from the HTTP request. This may differ from
    //   the host's native name.
    //
    // - The sharing status from the point of view of the current user.
    //
    // - The account name and display name for the entity owner. This info
    //   is useful to present in the client, but hard for the client to
    //   get on its own.
    //
    // - The ISO 8601 formatted creation and changed dates.
    //
    // These values are added in a raw entity field-like format, or they
    // are added as alternate values for a existing entity field.
    $content['host'] = [
      0 => [
        'value' => $this->currentRequest->getHttpHost(),
      ],
    ];

    $content['path'] = [
      0 => [
        'value' => $entity->getPath(),
      ],
    ];

    $uid = $entity->getOwnerId();
    $owner = User::load($uid);
    if ($owner !== NULL) {
      $content['uid'][0]['account-name'] = $owner->getAccountName();
      $content['uid'][0]['display-name'] = $owner->getDisplayName();
      $content['sharing-status'] = [
        0 => [
          'value' => $entity->getSharingStatus(),
        ],
      ];
    }
    else {
      $content['uid'][0]['account-name'] = 'unknown';
      $content['uid'][0]['display-name'] = 'unknown';
      $content['sharing-status'] = [
        0 => [
          'value' => 'unknown',
        ],
      ];
    }

    $content['created'][0]['formatted'] = date('c', $entity->getCreatedTime());
    $content['changed'][0]['formatted'] = date('c', $entity->getChangedTime());

    return $content;
  }

  /*--------------------------------------------------------------------
   *
   * Key-value pair pseudo-serialization.
   *
   *--------------------------------------------------------------------*/
  /**
   * Creates a key-value pair version of an entity.
   *
   * This method serializes an entity into an associative array of field
   * name keys and their field values. For complex fields, only the primary
   * value (used in renders) is included.
   *
   * This key-value array provides a simpler view of the entity, though it
   * lacks detail and is not compatible with further PATCH and POST requests.
   *
   * This method adds virtual fields to the entity that assist a REST
   * client in intepreting and using the entity's values:
   *
   * - 'host' - the current host name.
   * - 'user-account-name' - the entity owner's account name.
   * - 'user-display-name' - the entity owner's display name.
   * - 'sharing-status' - the entity's sharing status.
   * - 'path' - the entity's full path.
   * - 'created-date' - the ISO 8601 formatted creation date.
   * - 'changed-date' - the ISO 8601 formatted changed date.
   *
   * The entity should already have been cleaned to remove empty fields and
   * fields that cannot be viewed by the user.
   *
   * <B>Formatted timestamps</B><BR>
   * The raw numeric timestamps in the entity for created and changed
   * dates use a time basis that may be OS-specific. PHP's manual
   * claims that all timestamps follow the UNIX standard with starting
   * value at Jan 1 1970, but this may not be true. Windows uses Jan 1 1601
   * in the system and that may or may not be exposed in PHP. But
   * regardless, the REST client may be running on an OS that uses an
   * alternate time basis, and the client may not be using PHP. This
   * makes the raw numeric timestamps in the entity require conversion
   * to the client's time basis, which may or may not be trivial.
   *
   * A more general approach is to return an ISO 8601 formatted time.
   * Such a time has year, day, month, hours, minutes, seconds, and
   * the timezone. Providing this in the returned information enables
   * clients to parse a standard time and map it to their own needs.
   *
   * @param \Drupal\foldeshare\FolderShareInterface $entity
   *   The raw entity.
   *
   * @return array
   *   Returns an associative array with keys with field names and values
   *   that contain the simplified representation of the field's value.
   */
  private function formatKeyValueEntity(FolderShareInterface &$entity) {
    // Map the entity's fields to key-value pairs. This provides the
    // primary data for the response.
    $content = [];
    foreach ($entity as $fieldName => &$field) {
      $value = $this->createKeyValueFieldItemList($field);
      if ($value !== NULL) {
        $content[$fieldName] = $value;
      }
    }

    // Add virtual fields:
    //
    // - The current host name from the HTTP request. This may differ from
    //   the host's native name.
    //
    // - The sharing status from the point of view of the current user.
    //
    // - The account name and display name for the entity owner. This info
    //   is useful to present in the client, but hard for the client to
    //   get on its own.
    //
    // - The ISO 8601 formatted creation and changed dates.
    $content['host'] = $this->currentRequest->getHttpHost();

    $uid = $entity->getOwnerId();
    $owner = User::load($uid);
    if ($owner !== NULL) {
      $content['uid-account-name'] = $owner->getAccountName();
      $content['uid-display-name'] = $owner->getDisplayName();
      $content['sharing-status']   = $entity->getSharingStatus();
    }
    else {
      $content['uid-account-name'] = 'unknown';
      $content['uid-display-name'] = 'unknown';
      $content['sharing-status']   = 'unknown';
    }

    $content['path'] = $entity->getPath();

    $content['created-date'] = date('c', $entity->getCreatedTime());
    $content['changed-date'] = date('c', $entity->getChangedTime());

    return $content;
  }

  /**
   * Returns a simplified value for a field item list.
   *
   * If the field item list has one entry, the value for that entry is
   * returned.  Otherwise an array of entry values is returned.
   *
   * If the field item list is empty, a NULL is returned.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $fieldList
   *   The field item list whose values contribute to a key-value pair output.
   *
   * @return mixed
   *   Returns a value for the field item list.
   */
  private function createKeyValueFieldItemList(
    FieldItemListInterface &$fieldList) {

    // If the list is empty, return nothing.
    if ($fieldList->isEmpty() === TRUE) {
      return NULL;
    }

    // If the list has only one entry, return the representation of
    // that one entry. This is the most common case.
    if ($fieldList->count() === 1) {
      return $this->createKeyValueFieldItem($fieldList->first());
    }

    // Otherwise when the list has multiple entries, return an array
    // of their values.
    $values = [];
    foreach ($fieldList as $field) {
      $v = $this->createKeyValueFieldItem($field);
      if (empty($v) === FALSE) {
        $values[] = $v;
      }
    }

    return $values;
  }

  /**
   * Returns a simplified value for a field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $field
   *   The field item whose value contributes to a key-value pair output.
   *
   * @return string
   *   Returns a value for the field item.
   */
  private function createKeyValueFieldItem(FieldItemInterface $field) {
    if ($field->isEmpty() === TRUE) {
      return NULL;
    }

    return $field->getString();
  }

  /*--------------------------------------------------------------------
   *
   * Responses with an entity or entity list.
   *
   *--------------------------------------------------------------------*/
  /**
   * Modifies an entity to clean out empty or unviewable fields.
   *
   * This method loops through an entity's fields and:
   * - Removes fields with no values.
   * - Removes fields for which the user does not have 'view' access.
   *
   * In both cases, removed fields are set to NULL. It is *assumed* that
   * the caller will not save the entity, which could corrupt the
   * database with an invalid entity.
   *
   * @param \Drupal\foldeshare\FolderShareInterface $entity
   *   The entity whose fields should be cleared if they are not viewable.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns the same entity.
   */
  private function cleanEntity(FolderShareInterface &$entity) {
    // Start by cloning the entity. We'll modify and return the clone.
    //
    // It is important that the original entity not be modified since it
    // may have been loaded from the memory cache. In that case, the entity
    // object *IS* the object in the cache so if we modify it here, it will
    // corrupt the cache.
    $partialEntity = clone $entity;

    foreach ($partialEntity as $fieldName => &$field) {
      // Check access to the field.
      if ($field->access('view', NULL, FALSE) === FALSE) {
        $partialEntity->set($fieldName, NULL, FALSE);
        continue;
      }

      // Check for empty fields.
      if ($field->isEmpty() === TRUE) {
        $partialEntity->set($fieldName, NULL, FALSE);
        continue;
      }

      // Fields normally implement isEmpty() in a sane way to report
      // when a field is empty. But the comment module implements isEmpty()
      // to always return FALSE because it always has state for the entity,
      // including whether or not commenting is open or closed.
      //
      // For our purposes, though, a clean entity should omit a comment
      // field if the entity has no comments yet. This means we cannot
      // rely upon isEmpty().
      //
      // This is made trickier because the right way to check this is not
      // well-defined, and because the comment module may not be installed.
      $first = $field->get(0);
      if (is_a($first, 'Drupal\comment\Plugin\Field\FieldType\CommentItem') === TRUE) {
        // We have a comment field. It contains multiple values for
        // commenting open/closed, the most recent comment, and finally
        // the number of comments so far. We only care about the latter.
        if (intval($first->get('comment_count')->getValue()) === 0) {
          $partialEntity->set($fieldName, NULL, FALSE);
          continue;
        }
      }
    }

    return $partialEntity;
  }

  /**
   * Builds and returns a response containing a single entity.
   *
   * The entity is "cleaned" to remove non-viewable internal fields,
   * and then formatted properly using the selected return format.
   * A response containing the formatted entity is returned.
   *
   * @param \Drupal\foldershare\FolderShareInterface $entity
   *   A single entity to return, after cleaning and formatting.
   *
   * @return \Drupal\foldershare_rest\Plugin\rest\resource\UncacheableResponse
   *   Returns an uncacheable response.
   */
  private function formatEntityResponse(FolderShareInterface $entity) {
    //
    // Validate
    // --------
    // If there is no entity, return nothing.
    if ($entity === NULL) {
      return new UncacheableResponse(NULL, Response::HTTP_NO_CONTENT);
    }

    //
    // URL
    // ---
    // Create the URL for the updated entity.
    if (empty($entity->id()) === FALSE) {
      $url = $entity->toUrl(
        'canonical',
        ['absolute' => TRUE])->toString(TRUE);

      $headers = [
        // 'Location' => $url->getGeneratedUrl(),
      ];
    }
    else {
      $headers = [];
    }

    //
    // Entity access control again
    // ---------------------------
    // If the user has view permission for the entity (and they likely do
    // if they had update permission checked earlier), then we can return
    // the updated entity. Otherwise not.
    if ($entity->access('view', NULL, FALSE) === FALSE) {
      // This is odd that they can edit, but not view.
      return new UncacheableResponse(NULL, Response::HTTP_NO_CONTENT, $headers);
    }

    //
    // Clean entity
    // ------------
    // A loaded entity has all fields, but only some fields are user-visible.
    // "Clean" the entity of non-viewable fields by deleting them from the
    // entity.
    $partialEntity = $this->cleanEntity($entity);

    //
    // Return entity
    // -------------
    // Based upon the return format, return the entity.
    switch ($this->getAndValidateReturnFormat()) {
      default:
      case 'full':
        // Return as full entity.
        $response = new UncacheableResponse(
          $this->formatRawEntity($partialEntity),
          Response::HTTP_OK,
          $headers);
        break;

      case 'keyvalue':
        // Return as key-value pairs.
        $response = new UncacheableResponse(
          $this->formatKeyValueEntity($partialEntity),
          Response::HTTP_OK,
          $headers);
        break;
    }

    if ($partialEntity->isNew() === FALSE) {
      $this->addLinkHeaders($partialEntity, $response);
    }

    return $response;
  }

  /**
   * Builds and returns a response containing a list of entities.
   *
   * The entities are "cleaned" to remove non-viewable internal fields,
   * and then formatted properly using the selected return format.
   * A response containing the formatted entity is returned.
   *
   * @param \Drupal\foldershare\FolderShareInterface[] $entities
   *   A list of entities to return, after cleaning and formatting.
   *
   * @return \Drupal\foldershare_rest\Plugin\rest\resource\UncacheableResponse
   *   Returns an uncacheable response.
   */
  private function formatEntityListResponse(array $entities) {
    //
    // Clean entities
    // --------------
    // Loaded entities have all fields, but only some fields are user-visible.
    // "Clean" the entities of non-viewable fields by deleting them from the
    // entity.
    if (empty($entities) === TRUE) {
      return new UncacheableResponse(NULL, Response::HTTP_NO_CONTENT);
    }

    $partialEntities = [];
    foreach ($entities as &$entity) {
      $partialEntities[] = $this->cleanEntity($entity);
    }

    if (empty($partialEntities) === TRUE) {
      return new UncacheableResponse(NULL, Response::HTTP_NO_CONTENT);
    }

    //
    // Return entities
    // ---------------
    // Based upon the return format, return the entities.
    switch ($this->getAndValidateReturnFormat()) {
      default:
      case 'full':
        // Return as full entities.
        $e = [];
        foreach ($partialEntities as &$entity) {
          $e[] = $this->formatRawEntity($entity);
        }
        return new UncacheableResponse($e);

      case 'keyvalue':
        // Return as key-value pairs.
        $kv = [];
        foreach ($partialEntities as &$entity) {
          $kv[] = $this->formatKeyValueEntity($entity);
        }
        return new UncacheableResponse($kv);
    }
  }

}
