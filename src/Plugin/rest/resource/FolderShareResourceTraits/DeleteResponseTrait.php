<?php

namespace Drupal\foldershare_rest\Plugin\rest\resource\FolderShareResourceTraits;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Drupal\foldershare\Constants;
use Drupal\foldershare\ManagePaths;
use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\Entity\Exception\NotFoundException;
use Drupal\foldershare\Entity\Exception\LockException;
use Drupal\foldershare\Entity\Exception\ValidationException;

use Drupal\foldershare_rest\Plugin\rest\resource\UncacheableResponse;

/**
 * Respond to DELETE requests.
 *
 * This trait includes methods that implement a generic response to
 * HTTP DELETE requests.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare REST module and used to define
 * features of the FolderShareResource entity class. It is a mechanism to group
 * functionality to improve code management.
 *
 * @ingroup foldershare_rest
 */
trait DeleteResponseTrait {

  /*--------------------------------------------------------------------
   *
   * Generic.
   *
   *--------------------------------------------------------------------*/
  /**
   * Responds to an HTTP DELETE requests.
   *
   * The HTTP request contains:
   * - X-FolderShare-Delete-Operation = "delete-file", "delete-folder",
   *   "delete-folder-tree", or "delete-file-and-folder-tree".
   *
   * The HTTP response contains:
   * - A no-content message.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming HTTP POST request.
   *
   * @return \Drupal\foldershare_rest\Plugin\rest\resource\UncacheableResponse
   *   Returns an uncacheable response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws an exception if arguments are bad.
   */
  public function delete(Request $request) {
    //
    // Get the operation
    // -----------------
    // Get the specific DELETE operation to perform from the HTTP header,
    // if any. If a header is not provided, default to getting an entity.
    $operation = $this->getAndValidateDeleteOperation();

    //
    // Get source
    // ----------
    // Parse the ?id=ID query from the request path, if any, to get an
    // entity ID.
    //
    // Get the source entity path using the HTTP header, if any.
    $queries = explode('&', $request->getQueryString());
    $id = NULL;
    foreach ($queries as $query) {
      if (strstr($query, 'id=') !== FALSE) {
        $id = intval(substr($query, 3));
        break;
      }
    }

    $source = $this->getSourcePath();

    if ($id === NULL && empty($source) === TRUE) {
      // Neither an entity ID or a source path header was provided.
      // Developer exception text. Do not translate it.
      throw new BadRequestHttpException(
        "Malformed request. Web service delete request is missing an entity to delete.");
    }

    if (empty($source) === FALSE) {
      // A path was provided in the HTTP header. Parse it to get an
      // entity ID. If an entity ID was also provided on the request,
      // ignore it in favor of the header.
      try {
        $id = ManagePaths::findPathItemId($source);
      }
      catch (NotFoundException $e) {
        // Developer exception text. Do not translate it.
        throw new NotFoundHttpException($e->getMessage());
      }
      catch (ValidationException $e) {
        // Developer exception text. Do not translate it.
        throw new BadRequestHttpException(
          'Malformed request. ' . $e->getMessage());
      }
      catch (\Exception $e) {
        // Developer exception text. Do not translate it.
        throw new BadRequestHttpException(
          'Malformed request. ' . $e->getMessage());
      }
    }

    // Load the entity. Check for 'delete' access.
    $entity = $this->loadAndValidateEntity($id, (string) t("to delete"));
    if ($entity->isSystemHidden() === TRUE) {
      // User-facing exception text. Translate it.
      throw new NotFoundHttpException(
        FolderShare::getStandardHiddenMessage($entity->getName()));
    }

    if ($entity->isSystemDisabled() === TRUE) {
      // User-facing exception text. Translate it.
      throw new ConflictHttpException(
        FolderShare::getStandardDisabledMessage('deleted', $entity->getName()));
    }

    //
    // Prevent deletes of shared root items
    // ------------------------------------
    // Check the source and verify the item can be deleted. This will fail if:
    // - The source is a shared root and the current user is not the owner and
    //   not a content admin.
    if ($entity->isRootItem() === TRUE &&
        $entity->isAccessShared() === TRUE) {
      $ownerId = $entity->getOwnerId();
      if ($ownerId !== (int) $$this->currentUser->id() &&
          $this->currentUser->hasPermission(Constants::ADMINISTER_PERMISSION) === FALSE) {
        // User-facing exception text. Translate it.
        $message = t(
          "The item '@name' cannot be deleted. \r\nThe item is shared with you, but owned by another user.",
          [
            '@name' => $entity->getName(),
          ]);
        throw new ConflictHttpException($message);
      }
    }

    //
    // Delete
    // ------
    // Delete the entity. If the entity is a folder, this recurses to delete
    // all children as well.
    switch ($operation) {
      default:
      case 'delete-file':
        // Delete a file. Fail if the entity is not a file.
        if ($entity->isFolder() === TRUE) {
          // User-facing exception text. Translate it.
          $message = t(
            "Malformed request. The '@operation' web service only deletes files, but a folder was provided.",
            [
              '@operation' => $operation,
            ]);
          throw new BadRequestHttpException($message);
        }
        break;

      case 'delete-folder':
        // Delete a folder, without recursion. Fail if the entity is not a
        // folder or the folder is not empty.
        if ($entity->isFolder() === FALSE) {
          // User-facing exception text. Translate it.
          $message = t(
            "Malformed request. The '@operation' web service only deletes folders, but a file was provided.",
            [
              '@operation' => $operation,
            ]);
          throw new BadRequestHttpException($message);
        }

        if (empty($entity->findChildrenIds()) === FALSE) {
          // User-facing exception text. Translate it.
          $message = t(
            "The '@name' folder cannot be deleted. It must be emptied first.",
            [
              '@name' => $entity->getName(),
            ]);
          throw new BadRequestHttpException($message);
        }
        break;

      case 'delete-folder-tree':
        // Delete a folder, with recursion. Fail if the entity is not a folder.
        if ($entity->isFolder() === FALSE) {
          // When deleting a folder, the entity must be a folder.
          // User-facing exception text. Translate it.
          $message = t(
            "Malformed request. The '@operation' web service only deletes files, but a folder was provided.",
            [
              '@operation' => $operation,
            ]);
          throw new BadRequestHttpException($message);
        }
        break;

      case 'delete-file-or-folder':
        // Delete a file or folder, without recursion. Fail if the entity
        // is a folder and the folder is not empty.
        if (empty($entity->findChildrenIds()) === FALSE) {
          // User-facing exception text. Translate it.
          $message = t(
            "The '@name' folder cannot be deleted. It must be emptied first.",
            [
              '@name' => $entity->getName(),
            ]);
          throw new BadRequestHttpException($message);
        }
        break;

      case 'delete-file-or-folder-tree':
        // Delete a file or folder, with recursion. This will delete anything.
        // Fall through.
        break;
    }

    try {
      // Delete!
      $entity->delete();

      return new UncacheableResponse(NULL, Response::HTTP_NO_CONTENT);
    }
    catch (LockException $e) {
      // Developer exception text. Do not translate it.
      throw new ConflictHttpException($e->getMessage());
    }
    catch (\Exception $e) {
      // Developer exception text. Do not translate it.
      throw new HttpException(
        Response::HTTP_INTERNAL_SERVER_ERROR,
        'Internal Server Error.',
        $e);
    }
  }

}
