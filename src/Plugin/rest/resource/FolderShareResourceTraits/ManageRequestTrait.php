<?php

namespace Drupal\foldershare_rest\Plugin\rest\resource\FolderShareResourceTraits;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Drupal\user\Entity\User;

use Drupal\foldershare\FolderShareInterface;
use Drupal\foldershare\ManagePaths;
use Drupal\foldershare\Utilities\FileUtilities;
use Drupal\foldershare\Utilities\UserUtilities;
use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\Entity\FolderShareAccessControlHandler;
use Drupal\foldershare\Entity\Exception\NotFoundException;
use Drupal\foldershare\Entity\Exception\ValidationException;

use Drupal\foldershare_rest\Plugin\rest\resource\FolderShareResourceConstants;

/**
 * Manage HTTP request headers, including getting and validating values.
 *
 * This trait includes internal methods used to manage custom HTTP header
 * values used to communicate operators and operands.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare REST module and used to define
 * features of the FolderShareResource entity class. It is a mechanism to group
 * functionality to improve code management.
 *
 * @ingroup foldershare_rest
 */
trait ManageRequestTrait {

  /*--------------------------------------------------------------------
   *
   * Operations.
   *
   *--------------------------------------------------------------------*/
  /**
   * Gets, validates, and returns an operation from the request header.
   *
   * The HEADER_GET_OPERATION header is retrieved. If not found,
   * the default operation is returned. Otherwise the header's operation
   * is validated and returned.
   *
   * An exception is thrown if the operation is not recognized.
   *
   * @param string $header
   *   The name of the HTTP header to check.
   * @param array $allowed
   *   The array of allowed HTTP header values.
   * @param string $default
   *   The default value if the header is not found.
   *
   * @return string
   *   Returns the name of the operation, or the default if no operation
   *   was specified.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws a BadRequestHttpException if an unknown operation is found.
   *
   * @see ::getAndValidateGetOperation()
   * @see ::getAndValidatePatchOperation()
   * @see ::getAndValidatePostOperation()
   */
  private function getAndValidateOperation(
    string $header,
    array $allowed,
    string $default) {

    // Get the request's headers.
    $requestHeaders = $this->currentRequest->headers;

    // Get the header. If not found, return the default.
    if ($requestHeaders->has($header) === FALSE) {
      return $default;
    }

    // Get and validate the operation.
    $operation = $requestHeaders->get($header);
    if (is_string($operation) === TRUE &&
        in_array($operation, $allowed, TRUE) === TRUE) {
      return $operation;
    }

    // Fail.
    // Developer exception text. Do not translate it.
    throw new BadRequestHttpException(
      'Malformed request. The requested "' . $operation . '" web service is not recognized.');
  }

  /**
   * Gets, validates, and returns the GET operation from the request header.
   *
   * The HEADER_GET_OPERATION header is retrieved. If not found,
   * the default operation is returned. Otherwise the header's operation
   * is validated and returned.
   *
   * An exception is thrown if the operation is not recognized.
   *
   * @return string
   *   Returns the name of the operation, or the default if no operation
   *   was specified.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws a BadRequestHttpException if an unknown operation is found.
   *
   * @see ::HEADER_GET_OPERATION
   * @see ::GET_OPERATIONS
   * @see ::DEFAULT_GET_OPERATION
   */
  private function getAndValidateGetOperation() {
    return $this->getAndValidateOperation(
      FolderShareResourceConstants::HEADER_GET_OPERATION,
      FolderShareResourceConstants::GET_OPERATIONS,
      FolderShareResourceConstants::DEFAULT_GET_OPERATION);
  }

  /**
   * Gets, validates, and returns the DELETE operation from the request header.
   *
   * The HEADER_DELETE_OPERATION header is retrieved. If not found,
   * the default operation is returned. Otherwise the header's operation
   * is validated and returned.
   *
   * An exception is thrown if the operation is not recognized.
   *
   * @return string
   *   Returns the name of the operation, or the default if no operation
   *   was specified.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws a BadRequestHttpException if an unknown operation is found.
   *
   * @see ::HEADER_DELETE_OPERATION
   * @see ::DELETE_OPERATIONS
   * @see ::DEFAULT_DELETE_OPERATION
   */
  private function getAndValidateDeleteOperation() {
    return $this->getAndValidateOperation(
      FolderShareResourceConstants::HEADER_DELETE_OPERATION,
      FolderShareResourceConstants::DELETE_OPERATIONS,
      FolderShareResourceConstants::DEFAULT_DELETE_OPERATION);
  }

  /**
   * Gets, validates, and returns the PATCH operation from the request header.
   *
   * The HEADER_PATCH_OPERATION header is retrieved. If not found,
   * the default operation is returned. Otherwise the header's operation
   * is validated and returned.
   *
   * An exception is thrown if the operation is not recognized.
   *
   * @return string
   *   Returns the name of the operation, or the default if no operation
   *   was specified.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws a BadRequestHttpException if an unknown operation is found.
   *
   * @see ::HEADER_PATCH_OPERATION
   * @see ::PATCH_OPERATIONS
   * @see ::DEFAULT_PATCH_OPERATION
   */
  private function getAndValidatePatchOperation() {
    return $this->getAndValidateOperation(
      FolderShareResourceConstants::HEADER_PATCH_OPERATION,
      FolderShareResourceConstants::PATCH_OPERATIONS,
      FolderShareResourceConstants::DEFAULT_PATCH_OPERATION);
  }

  /**
   * Gets, validates, and returns the POST operation from the request header.
   *
   * The HEADER_POST_OPERATION header is retrieved. If not found,
   * the default operation is returned. Otherwise the header's operation
   * is validated and returned.
   *
   * An exception is thrown if the operation is not recognized.
   *
   * @return string
   *   Returns the name of the operation, or the default if no operation
   *   was specified.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws a BadRequestHttpException if an unknown operation is found.
   *
   * @see ::HEADER_POST_OPERATION
   * @see ::POST_OPERATIONS
   * @see ::DEFAULT_POST_OPERATION
   */
  private function getAndValidatePostOperation() {
    return $this->getAndValidateOperation(
      FolderShareResourceConstants::HEADER_POST_OPERATION,
      FolderShareResourceConstants::POST_OPERATIONS,
      FolderShareResourceConstants::DEFAULT_POST_OPERATION);
  }

  /**
   * Gets and validates the filename for the POST request.
   *
   * The filename for a POST is provided in the "content-disposition"
   * header within the request. This function gets that header,
   * parses out the filename, and returns it.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The HTTP request to parse.
   *
   * @return string
   *   Returns the filename from the request header.
   */
  private function getAndValidatePostFilename(Request $request) {
    // Look for the "content-disposition" header.
    if ($request->headers->has('content-disposition') === FALSE) {
      // Developer exception text. Do not translate it.
      throw new BadRequestHttpException(
        'Malformed request. The required "content-disposition" header providing a new file/folder name is missing.');
    }

    // Parse the header. The format should be:
    // - "Content-disposition: attachment; filename="FILENAME"
    // - "Content-disposition: attachment; filename*="FILENAME"
    //
    // The second form includes a "*" after filename and indicates that
    // the filename uses the encoding in RFC 4987. This is a fairly obscure
    // feature that is not supported.
    $header = $request->headers->get('content-disposition');
    if (preg_match('@\bfilename.*=\"(.*)\"@', $header, $matches) !== 1) {
      // Developer exception text. Do not translate it.
      throw new BadRequestHttpException(
        'Malformed request. The required filename in "content-disposition" header is missing.');
    }

    // Return the filename.
    return FileUtilities::basename($matches[1]);
  }

  /*--------------------------------------------------------------------
   *
   * Misc. operands.
   *
   *--------------------------------------------------------------------*/
  /**
   * Gets, validates, and returns the last modified date from the header.
   *
   * The last modified date is optionally provided in the "last-modified"
   * header within the request. This function gets that header,
   * parses out the last modified date and converts to to a timestamp.
   *
   * @return int
   *   Returns the timestamp from the request header, or -1 if there
   *   was no value.
   */
  private function getAndValidateLastModified() {
    // Get the request's headers.
    $requestHeaders = $this->currentRequest->headers;

    // Look for the "last-modified" header.
    if ($requestHeaders->has('last-modified') === FALSE) {
      // Missing.
      return (-1);
    }

    // Parse the header. The format should be:
    // - "Last-modified: dayname, day month year hour:minute:second GMT".
    $header = $requestHeaders->get('last-modified');
    $modified = @strtotime($header);
    if ($modified === FALSE) {
      // Malformed?
      return (-1);
    }

    return $modified;
  }

  /**
   * Gets, validates, and returns the return type from the request header.
   *
   * The current request's headers are checked for HEADER_RETURN_FORMAT.
   * If the header is found, it's value is validated against a list of
   * well-known return types and returned. If the header is not found,
   * a default return type is returned.
   *
   * @return string
   *   Returns the name of the return type, or the default if no return type
   *   was specified.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws a BadRequestHttpException if an unknown return type is found.
   *
   * @see ::HEADER_RETURN_FORMAT
   * @see ::RETURN_FORMATS
   * @see ::DEFAULT_RETURN_FORMAT
   */
  private function getAndValidateReturnFormat() {
    // Get the request's headers.
    $requestHeaders = $this->currentRequest->headers;

    // If no return type is specified, return the default.
    if ($requestHeaders->has(FolderShareResourceConstants::HEADER_RETURN_FORMAT) === FALSE) {
      return FolderShareResourceConstants::DEFAULT_RETURN_FORMAT;
    }

    // Get and validate the return type.
    $returnFormat = $requestHeaders->get(FolderShareResourceConstants::HEADER_RETURN_FORMAT);
    if (is_string($returnFormat) === TRUE &&
        in_array($returnFormat, FolderShareResourceConstants::RETURN_FORMATS, TRUE) === TRUE) {
      return $returnFormat;
    }

    // Fail.
    // Developer exception text. Do not translate it.
    throw new BadRequestHttpException(
      'Malformed request. The requested "' . $returnFormat . '" web service return format is not recognized.');
  }

  /**
   * Gets, validates, and returns the User entity from the request header.
   *
   * The current request's headers are checked for HEADER_USER.
   * If the header is found, it's value is tested as a user account name
   * or a numeric User entity ID. The User entity is found, loaded,
   * and returned.
   *
   * @return string
   *   Returns the name of the return type, or the default if no return type
   *   was specified.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws a BadRequestHttpException if an unknown user is found.
   *
   * @see ::HEADER_USER
   */
  private function getAndValidateUser() {
    // Get the request's headers.
    $requestHeaders = $this->currentRequest->headers;

    // If no user is specified, fail.
    if ($requestHeaders->has(FolderShareResourceConstants::HEADER_USER) === FALSE) {
      // Developer exception text. Do not translate it.
      throw new BadRequestHttpException(
        "Malformed request. A necessary user name or ID was not provided.");
    }

    // Get and validate the user.
    $userValue = $requestHeaders->get(FolderShareResourceConstants::HEADER_USER);
    $userId = UserUtilities::findUserByAccountName($userValue);
    if ($userId === (-1)) {
      // The user value wasn't found as an account name. Is it an integer?
      if (mb_ereg_match('^([0-9]+)$', $userValue) === FALSE) {
        $userId = (-1);
      }
      else {
        $userId = intval($userValue);
      }
    }

    $user = User::load($userId);
    if ($user !== NULL) {
      return $user;
    }

    // Fail.
    // Developer exception text. Do not translate it.
    throw new BadRequestHttpException(
      'Malformed request. The requested user "' . $userValue . '" is not recognized.');
  }

  /*--------------------------------------------------------------------
   *
   * Entity operands.
   *
   *--------------------------------------------------------------------*/
  /**
   * Loads and validates access to the indicated entity.
   *
   * @param int $id
   *   The entity ID for a FolderShare entity.
   * @param string $operation
   *   The operation being performed (e.g. delete, view). This should
   *   already have been translated to the user's language choice.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns the loaded entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws a NotFoundHttpException if the entity ID is bad, and
   *   an AccessDeniedHttpException if the user does not have access.
   */
  private function loadAndValidateEntity(int $id, string $operation) {

    // Load the entity using the ID.
    $entity = FolderShare::load($id);
    if ($entity === NULL) {
      if (empty($operation) === TRUE) {
        $operation = t('for web services');
      }

      // User-facing exception text. Translate it.
      $message = t(
        "The requested entity ID '@id' for @operation could not be found.",
        [
          '@id'        => $id,
          '@operation' => $operation,
        ]);
      throw new NotFoundHttpException($message);
    }

    if ($entity->isSystemHidden() === TRUE) {
      // Hidden items do not exist.
      // User-facing exception text. Translate it.
      throw new NotFoundHttpException(
        FolderShare::getStandardHiddenMessage($entity->getName()));
    }

    // Check if the user has 'view' access to the entity.
    $access = $entity->access('view', NULL, TRUE);
    if ($access->isAllowed() === FALSE) {
      // No access. If a reason was not provided, use a default.
      $message = $access->getReason();
      if (empty($message) === TRUE) {
        $message = $this->getDefaultAccessDeniedMessage('view');
      }

      // User-facing exception text. Translate it.
      throw new AccessDeniedHttpException($message);
    }

    return $entity;
  }

  /*--------------------------------------------------------------------
   *
   * Path handling.
   *
   *--------------------------------------------------------------------*/
  /**
   * Gets the source path from the request header.
   *
   * @return string
   *   Returns the source path, or an empty string if the request header
   *   was not set or it had an empty value.
   *
   * @see ::HEADER_SOURCE_PATH
   */
  private function getSourcePath() {
    $requestHeaders = $this->currentRequest->headers;
    if ($requestHeaders->has(FolderShareResourceConstants::HEADER_SOURCE_PATH) === FALSE) {
      return '';
    }

    $value = $requestHeaders->get(FolderShareResourceConstants::HEADER_SOURCE_PATH);
    if (empty($value) === TRUE) {
      return '';
    }

    return rawurldecode($value);
  }

  /**
   * Gets the destination path from the request header.
   *
   * @return string
   *   Returns the source path, or an empty string if the request header
   *   was not set or it had an empty value.
   *
   * @see ::HEADER_DESTINATION_PATH
   */
  private function getDestinationPath() {
    $requestHeaders = $this->currentRequest->headers;
    if ($requestHeaders->has(FolderShareResourceConstants::HEADER_DESTINATION_PATH) === FALSE) {
      return '';
    }

    $value = $requestHeaders->get(FolderShareResourceConstants::HEADER_DESTINATION_PATH);
    if (empty($value) === TRUE) {
      return '';
    }

    return rawurldecode($value);
  }

  /*--------------------------------------------------------------------
   *
   * Source and destination entities.
   *
   *--------------------------------------------------------------------*/
  /**
   * Gets the source entity indicated by a URL entity ID or source path.
   *
   * If there is no source and $required is TRUE, an exception is thrown.
   * Otherwise a NULL is returned when there is no source.
   *
   * @param int $id
   *   (optional, default = EMPTY_ITEM_ID) The entity ID if there is no
   *   source path. A negative or EMPTY_ITEM_ID value indicates that no
   *   entity ID was provided.
   * @param bool $required
   *   (optional, default = TRUE) Indicate if the source entity must exist.
   *   If so and the entity cannot be found, an exception is thrown. Otherwise
   *   a NULL is returned.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns the loaded source entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws an exception if the source entity is required and there is
   *   no source found.
   */
  private function getSourceAndValidate(
    int $id = FolderShareResourceConstants::EMPTY_ITEM_ID,
    bool $required = TRUE) {

    // Get the source path, if any.
    $sourcePath = $this->getSourcePath();

    // If the source path exists, parse it to get the entity ID. Use that
    // ID to override the incoming entity ID.
    if (empty($sourcePath) === FALSE) {
      try {
        $id = ManagePaths::findPathItemId($sourcePath);
      }
      catch (NotFoundException $e) {
        // Developer exception text. Do not translate it.
        throw new NotFoundHttpException($e->getMarkup());
      }
      catch (ValidationException $e) {
        // Developer exception text. Do not translate it.
        throw new BadRequestHttpException(
          'Malformed request. ' . $e->getMessage());
      }
      catch (\Exception $e) {
        // Developer exception text. Do not translate it.
        throw new BadRequestHttpException(
          'Malformed request. ' . $e->getMessage());
      }
    }

    // If there is no entity ID, throw an exception if one is required.
    if ($id < 0) {
      if ($required === TRUE) {
        // Developer exception text. Do not translate it.
        throw new BadRequestHttpException(
          "Malformed request. The path to the source item is empty.");
      }

      return NULL;
    }

    // Load the entity.
    $entity = FolderShare::load($id);
    if ($entity === NULL) {
      if ($required === TRUE) {
        // User-facing exception text. Translate it.
        $message = t(
          "The requested entity ID '@id' could not be found.",
          [
            '@id' => $id,
          ]);
        throw new NotFoundHttpException($message);
      }

      return NULL;
    }

    if ($entity->isSystemHidden() === TRUE) {
      // Hidden items do not exist.
      // User-facing exception text. Translate it.
      throw new NotFoundHttpException(
        FolderShare::getStandardHiddenMessage($entity->getName()));
    }

    if ($entity->isSystemDisabled() === TRUE) {
      // Disabled items cannot be used.
      // User-facing exception text. Translate it.
      throw new ConflictHttpException(
        FolderShare::getStandardDisabledMessage('used', $entity->getName()));
    }

    return $entity;
  }

  /**
   * Checks for user access to the indicated entity.
   *
   * Access is checked for the requested operation. If access is denied,
   * an exception is thrown with an appropriate message.
   *
   * @param \Drupal\foldeshare\FolderShareInterface $entity
   *   The entity to check for access. If NULL, the operation must be 'create'
   *   and access is checked for creating a root item.
   * @param string $operation
   *   The access operation to check for.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws an exception if access is denied.
   */
  private function validateAccess(
    FolderShareInterface $entity = NULL,
    string $operation = '') {

    // If there is an entity, check the entity directly for access.
    if ($entity !== NULL) {
      $access = $entity->access($operation, NULL, TRUE);

      if ($access->isAllowed() === TRUE) {
        // Access allowed.
        return;
      }

      // No access. If a reason was not provided, use a default.
      $message = $access->getReason();
      if (empty($message) === TRUE) {
        $message = $this->getDefaultAccessDeniedMessage($operation);
      }

      // Access denied.
      // User-facing exception text. Translate it.
      throw new AccessDeniedHttpException($message);
    }

    // Otherwise there is no entity.
    $summary = FolderShareAccessControlHandler::getRootAccessSummary(
      FolderShareInterface::USER_ROOT_LIST,
      NULL);
    if (isset($summary[$operation]) === TRUE &&
        $summary[$operation] === TRUE) {
      // Access allowed.
      return;
    }

    // Access denied.
    // User-facing exception text. Translate it.
    throw new AccessDeniedHttpException(
      $this->getDefaultAccessDeniedMessage($operation));
  }

}
