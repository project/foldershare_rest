<?php

namespace Drupal\foldershare_rest\Plugin\rest\resource\FolderShareResourceTraits;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Drupal\foldershare\ManagePaths;
use Drupal\foldershare\FolderShareInterface;
use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\Entity\FolderShareAccessControlHandler;
use Drupal\foldershare\Entity\Exception\ValidationException;
use Drupal\foldershare\Entity\Exception\NotFoundException;
use Drupal\foldershare\Entity\Exception\LockException;

use Drupal\foldershare_rest\Plugin\rest\resource\FolderShareResourceConstants;
use Drupal\foldershare_rest\Plugin\rest\resource\UncacheableResponse;

/**
 * Respond to HTTP POST requests.
 *
 * This trait includes methods that implement generic and operation-specific
 * responses to HTTP POST requests.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare REST module and used to define
 * features of the FolderShareResource entity class. It is a mechanism to group
 * functionality to improve code management.
 *
 * @ingroup foldershare_rest
 */
trait PostResponseTrait {

  /*--------------------------------------------------------------------
   *
   * Generic.
   *
   *--------------------------------------------------------------------*/
  /**
   * Responds to HTTP POST requests to create a new FolderShare entity.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming HTTP POST request.
   *
   * @return \Drupal\foldershare_rest\Plugin\rest\resource\UncacheableResponse
   *   Returns an uncacheable response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws an exception if arguments are bad.
   *
   * @todo Implement a post to create a new media entity.
   * @todo Implement a post to create a new object entity.
   */
  public function post(Request $request) {
    //
    // Get operation
    // -------------
    // Use the HTTP header to get the POST operation, if any.
    $filename = $this->getAndValidatePostFilename($request);
    $operation = $this->getAndValidatePostOperation();

    //
    // Dispatch
    // --------
    // Handle each of the POST operations.
    switch ($operation) {
      case 'new-rootfolder':
        return $this->postNewRootFolder($filename);

      case 'new-folder':
        return $this->postNewFolder($filename);

      case 'new-file':
        return $this->postNewFile($filename, TRUE);

      case 'new-file-no-overwrite':
        return $this->postNewFile($filename, FALSE);

      case 'new-rootfile':
        return $this->postNewRootFile($filename, TRUE);

      case 'new-rootfile-no-overwrite':
        return $this->postNewRootFile($filename, FALSE);

      case 'new-media':
        // @todo Implement creating new media entities.
        throw new HttpException(
          Response::HTTP_NOT_IMPLEMENTED,
          'Requests to create media items not yet supported.');

      case 'new-object':
        // @todo Implement creating new object entities.
        throw new HttpException(
          Response::HTTP_NOT_IMPLEMENTED,
          'Requests to create object items not yet supported.');
    }
  }

  /*--------------------------------------------------------------------
   *
   * Create folder.
   *
   *--------------------------------------------------------------------*/
  /**
   * Responds to an HTTP POST request to create a new root folder.
   *
   * The HTTP request contains:
   * - X-CSRF-Token: TOKEN.
   * - X-FolderShare-Post-Operation: new-rootfolder.
   * - X-FolderShare-Destination-Path: PATH.
   * - Content-Disposition: attachment; filename=NAME.
   * - Content-Type: application/octet-stream.
   * - Last-Modified: DATE.
   *
   * The HTTP response contains:
   * - The new entity's URL.
   *
   * @param string $name
   *   The name of the new folder.
   *
   * @return \Drupal\foldershare_rest\Plugin\rest\resource\UncacheableResponse
   *   Returns an uncacheable response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws an exception if arguments are bad.
   */
  private function postNewRootFolder(string $name) {

    $modified = $this->getAndValidateLastModified();

    //
    // Check create access
    // -------------------
    // Confirm that the user can create root items.
    $summary = FolderShareAccessControlHandler::getRootAccessSummary(
      FolderShareInterface::USER_ROOT_LIST,
      NULL);
    if (isset($summary['create']) === FALSE ||
        $summary['create'] === FALSE) {
      // Access denied.
      throw new AccessDeniedHttpException(
        $this->getDefaultAccessDeniedMessage('create'));
    }

    //
    // Create root folder
    // ------------------
    // Create a new root folder for the user. Do not auto-rename to avoid name
    // collisions. If the name collides, report it to the user as an error.
    try {
      $folder = FolderShare::createRootFolder($name, FALSE);

      if ($modified !== (-1)) {
        $folder->setChangedTime($modified);
        $folder->save();
      }
    }
    catch (LockException $e) {
      throw new ConflictHttpException($e->getMessage());
    }
    catch (ValidationException $e) {
      throw new BadRequestHttpException(
        $e->getMessage());
    }
    catch (\Exception $e) {
      throw new HttpException(
        Response::HTTP_INTERNAL_SERVER_ERROR,
        $e->getMessage());
    }

    //
    // URL
    // ---
    // Return the URL for the new entity.
    $url = $folder->toUrl(
      'canonical',
      ['absolute' => TRUE])->toString(TRUE);

    $headers = [
      'Location' => $url->getGeneratedUrl(),
    ];

    return new UncacheableResponse(NULL, Response::HTTP_CREATED, $headers);
  }

  /**
   * Responds to an HTTP POST request to create a new folder.
   *
   * The HTTP request contains:
   * - X-CSRF-Token: TOKEN.
   * - X-FolderShare-Post-Operation: new-folder.
   * - X-FolderShare-Destination-Path: PATH.
   * - Content-Disposition: attachment; filename=NAME.
   * - Content-Type: application/octet-stream.
   * - Last-Modified: DATE.
   *
   * The HTTP response contains:
   * - The new entity's URL.
   *
   * The destination path header should contain the path to a parent folder.
   * If the header is missing, the dummy entity must have a parent ID field
   * set.
   *
   * @param string $name
   *   The name of the new folder.
   *
   * @return \Drupal\foldershare_rest\Plugin\rest\resource\UncacheableResponse
   *   Returns an uncacheable response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws an exception if arguments are bad.
   */
  private function postNewFolder(string $name) {

    //
    // Find the parent
    // ---------------
    // Check for a header destination path. If found, it must provide
    // the path to the parent folder, which must exist.
    $destinationPath = $this->getDestinationPath();
    $modified = $this->getAndValidateLastModified();

    if (empty($destinationPath) === TRUE) {
      // Developer exception text. Do not translate it.
      throw new BadRequestHttpException(
        'Malformed request. Required "' . FolderShareResourceConstants::HEADER_DESTINATION_PATH . '" header for the new folder destination is missing.');
    }

    try {
      $parentId = ManagePaths::findPathItemId($destinationPath);
    }
    catch (NotFoundException $e) {
      throw new NotFoundHttpException($e->getMessage());
    }
    catch (ValidationException $e) {
      throw new BadRequestHttpException(
        'Malformed request. ' . $e->getMessage());
    }
    catch (\Exception $e) {
      throw new BadRequestHttpException(
        'Malformed request. ' . $e->getMessage());
    }

    if ($parentId === FALSE) {
      // Developer exception text. Do not translate it.
      throw new NotFoundHttpException(
        'Required new entity information has a bad parent entity ID "' . $parentId . '".');
    }

    $parent = FolderShare::load($parentId);
    if ($parent === NULL) {
      // Developer exception text. Do not translate it.
      throw new NotFoundHttpException(
        'Required new entity information has a bad parent entity ID "' . $parentId . '".');
    }

    if ($parent->isSystemHidden() === TRUE) {
      // Hidden items do not exist.
      throw new NotFoundHttpException(
        FolderShare::getStandardHiddenMessage($parent->getName()));
    }

    if ($parent->isSystemDisabled() === TRUE) {
      // Disabled items cannot be used.
      throw new ConflictHttpException(
        FolderShare::getStandardDisabledMessage('created', $parent->getName()));
    }

    //
    // Check create access
    // -------------------
    // Confirm that the user can create folders in the parent folder.
    $access = $parent->access('create', NULL, TRUE);
    if ($access->isAllowed() === FALSE) {
      // Access denied.
      $message = $access->getReason();
      if (empty($message) === TRUE) {
        $message = $this->getDefaultAccessDeniedMessage('create');
      }

      throw new AccessDeniedHttpException($message);
    }

    //
    // Create folder
    // -------------
    // Create a new folder in the parent. Do not auto-rename to avoid name
    // collisions. If the name collides, report it to the user as an error.
    try {
      $folder = $parent->createFolder($name, FALSE);

      if ($modified !== (-1)) {
        $folder->setChangedTime($modified);
        $folder->save();
      }
    }
    catch (LockException $e) {
      throw new ConflictHttpException($e->getMessage());
    }
    catch (ValidationException $e) {
      throw new BadRequestHttpException(
        $e->getMessage());
    }
    catch (\Exception $e) {
      throw new HttpException(
        Response::HTTP_INTERNAL_SERVER_ERROR,
        $e->getMessage());
    }

    //
    // URL
    // ---
    // Return the URL for the new entity.
    $url = $folder->toUrl(
      'canonical',
      ['absolute' => TRUE])->toString(TRUE);

    $headers = [
      'Location' => $url->getGeneratedUrl(),
    ];

    return new UncacheableResponse(NULL, Response::HTTP_CREATED, $headers);
  }

  /*--------------------------------------------------------------------
   *
   * Upload file.
   *
   *--------------------------------------------------------------------*/
  /**
   * Responds to an HTTP POST request to create a new root file.
   *
   * The dummy entity's 'name' field must have the name of the new file.
   *
   * The HTTP request contains:
   * - X-CSRF-Token: TOKEN
   * - X-FolderShare-Post-Operation: new-rootfile|new-rootfile-no-overwrite
   * - X-FolderShare-Destination-Path: PATH
   * - Content-Disposition: attachment; filename=NAME
   * - Content-Type: application/octet-stream
   * - Last-Modified: DATE
   *
   * The HTTP response contains:
   * - The new entity's URL.
   *
   * @param string $name
   *   The name of the new file.
   * @param bool $overwrite
   *   (optional, default = TRUE) When TRUE, any existing file is silently
   *   deleted and overwritten with the new file. When FALSE and there is
   *   an existing file, the new file is given a unique sequence number to
   *   avoid overwriting.
   *
   * @return \Drupal\foldershare_rest\Plugin\rest\resource\UncacheableResponse
   *   Returns an uncacheable response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws an exception if arguments are bad.
   */
  private function postNewRootFile(
    string $name,
    bool $overwrite = TRUE) {

    $modified = $this->getAndValidateLastModified();

    //
    // Check create access
    // -------------------
    // Confirm that the user can create root items.
    $summary = FolderShareAccessControlHandler::getRootAccessSummary(
      FolderShareInterface::USER_ROOT_LIST,
      NULL);
    if (isset($summary['create']) === FALSE ||
        $summary['create'] === FALSE) {
      // Access denied.
      // User-facing exception text. Translate it.
      throw new AccessDeniedHttpException(
        $this->getDefaultAccessDeniedMessage('create'));
    }

    //
    // Create file
    // -----------
    // Create a new folder in the parent. Do not auto-rename to avoid name
    // collisions. If the name collides, report it to the user as an error.
    try {
      $allowRename = !$overwrite;
      $file = FolderShare::addFileToRootFromInputStream(
        $name,
        $allowRename,
        (-1));

      if ($modified !== (-1)) {
        $file->setChangedTime($modified);
        $file->save();
      }
    }
    catch (LockException $e) {
      // Developer exception text. Do not translate it.
      throw new ConflictHttpException($e->getMessage());
    }
    catch (ValidationException $e) {
      // Developer exception text. Do not translate it.
      throw new BadRequestHttpException(
        $e->getMessage());
    }
    catch (\Exception $e) {
      // Developer exception text. Do not translate it.
      throw new HttpException(
        Response::HTTP_INTERNAL_SERVER_ERROR,
        $e->getMessage());
    }

    //
    // URL
    // ---
    // Return the URL for the new entity.
    $url = $file->toUrl(
      'canonical',
      ['absolute' => TRUE])->toString(TRUE);

    $headers = [
      'Location' => $url->getGeneratedUrl(),
    ];

    return new UncacheableResponse(NULL, Response::HTTP_CREATED, $headers);
  }

  /**
   * Responds to an HTTP POST request to create a new file.
   *
   * The HTTP request contains:
   * - X-CSRF-Token: TOKEN.
   * - X-FolderShare-Post-Operation: new-file|new-file-no-overwrite.
   * - X-FolderShare-Destination-Path: PATH.
   * - Content-Disposition: attachment; filename=NAME.
   * - Content-Type: application/octet-stream.
   * - Last-Modified: DATE.
   *
   * The HTTP response contains:
   * - The new entity's URL.
   *
   * The destination path header should contain the path to a parent folder.
   * If the header is missing, the dummy entity must have a parent ID field
   * set.
   *
   * @param string $name
   *   The name of the new file.
   * @param bool $overwrite
   *   (optional, default = TRUE) When TRUE, any existing file is silently
   *   deleted and overwritten with the new file. When FALSE and there is
   *   an existing file, the new file is given a unique sequence number to
   *   avoid overwriting.
   *
   * @return \Drupal\foldershare_rest\Plugin\rest\resource\UncacheableResponse
   *   Returns an uncacheable response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws an exception if arguments are bad.
   */
  private function postNewFile(
    string $name,
    bool $overwrite = TRUE) {

    //
    // Find the parent
    // ---------------
    // Check for a header destination path. If found, it must provide
    // the path to the parent folder, which must exist.
    $destinationPath = $this->getDestinationPath();
    $modified = $this->getAndValidateLastModified();

    if (empty($destinationPath) === TRUE) {
      // Developer exception text. Do not translate it.
      throw new BadRequestHttpException(
        'Malformed request. Required "' . FolderShareResourceConstants::HEADER_DESTINATION_HEADER . '" header for the new file destination is missing.');
    }

    try {
      $parentId = ManagePaths::findPathItemId($destinationPath);
    }
    catch (NotFoundException $e) {
      // Developer exception text. Do not translate it.
      throw new NotFoundHttpException($e->getMessage());
    }
    catch (ValidationException $e) {
      // Developer exception text. Do not translate it.
      throw new BadRequestHttpException(
        'Malformed request. ' . $e->getMessage());
    }
    catch (\Exception $e) {
      // Developer exception text. Do not translate it.
      throw new BadRequestHttpException(
        'Malformed request. ' . $e->getMessage());
    }

    if ($parentId === FALSE) {
      // Developer exception text. Do not translate it.
      throw new NotFoundHttpException(
        'Required new entity information has a bad parent entity ID "' . $parentId . '".');
    }

    $parent = FolderShare::load($parentId);
    if ($parent === NULL) {
      // Developer exception text. Do not translate it.
      throw new NotFoundHttpException(
        'Required new entity information has a bad parent entity ID "' . $parentId . '".');
    }

    if ($parent->isSystemHidden() === TRUE) {
      // Hidden items do not exist.
      // User-facing exception text. Translate it.
      throw new NotFoundHttpException(
        FolderShare::getStandardHiddenMessage($parent->getName()));
    }

    if ($parent->isSystemDisabled() === TRUE) {
      // Disabled items cannot be used.
      // User-facing exception text. Translate it.
      throw new ConflictHttpException(
        FolderShare::getStandardDisabledMessage('created', $parent->getName()));
    }

    //
    // Check create access
    // -------------------
    // Confirm that the user can create files in the parent folder.
    $access = $parent->access('create', NULL, TRUE);
    if ($access->isAllowed() === FALSE) {
      // Access denied.
      // User-facing exception text. Translate it.
      $message = $access->getReason();
      if (empty($message) === TRUE) {
        $message = $this->getDefaultAccessDeniedMessage('create');
      }

      throw new AccessDeniedHttpException($message);
    }

    //
    // Create file
    // -----------
    // Create a new folder in the parent. Do not auto-rename to avoid name
    // collisions. If the name collides, report it to the user as an error.
    try {
      $allowRename = !$overwrite;
      $file = $parent->addFileFromInputStream(
        $name,
        $allowRename,
        (-1));

      if ($modified !== (-1)) {
        $file->setChangedTime($modified);
        $file->save();
      }
    }
    catch (LockException $e) {
      // Developer exception text. Do not translate it.
      throw new ConflictHttpException($e->getMessage());
    }
    catch (ValidationException $e) {
      // Developer exception text. Do not translate it.
      throw new BadRequestHttpException(
        $e->getMessage());
    }
    catch (\Exception $e) {
      // Developer exception text. Do not translate it.
      throw new HttpException(
        Response::HTTP_INTERNAL_SERVER_ERROR,
        $e->getMessage());
    }

    //
    // URL
    // ---
    // Return the URL for the new entity.
    $url = $file->toUrl(
      'canonical',
      ['absolute' => TRUE])->toString(TRUE);

    $headers = [
      'Location' => $url->getGeneratedUrl(),
    ];

    return new UncacheableResponse(NULL, Response::HTTP_CREATED, $headers);
  }

}
