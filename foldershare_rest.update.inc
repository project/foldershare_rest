<?php

/**
 * @file
 * Implements hooks to update the module.
 *
 * Note that comments on update functions are parsed and shown to the
 * site admin to describe an update before it is applied.
 */

/*----------------------------------------------------------------------
 *
 * 1.0.0 to 1.1.0
 *
 *----------------------------------------------------------------------*/

/**
 * General update.
 *
 * This mandatory update flushes the cache to insure that all plugins
 * and other related resources are uptodate.
 */
function foldershare_rest_update_8001() {
  // This update is intentionally empty. Its effect is to cause all
  // caches to be flushed, which will pick up changes to the entity type.
  return t('Update complete.');
}
